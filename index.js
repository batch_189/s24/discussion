

// EXPONENT OPERSTOR

    const firstNum = 8 ** 2;
    console.log(firstNum);

    const secondNum = Math.pow(8, 2); // This is before ES6
    console.log(secondNum);

// 5 raised to the power of 5
    const thirdNum = 5 ** 5
    console.log(thirdNum);

// TEMPLATE LITERALS
    /*
        Allows us to write string without using the concatenation operator (+)
    */

    let name = "Nehemiah";

// Pre- Template Literal string
// Using single quote('')
    let message = 'Hello' + name + 'Welcome to programming!';
    console.log("Message without template literals: " + message);
    console.log()

// String Using Template Literal
// Uses the backticks (``)
    message = `Hello ${name}. Welcome to programming!.`
    console.log(message);

    let anotherMessage = `${name} attended a math competition.
    He won it by solving the problem 8**2 eith the solution og 64.`
    console.log(anotherMessage);

    anotherMessage = "\n" + name + ' attended a math competition. \n he won it by solving the problem 8**2 with the solution of ' + firstNum + ". \n"
    console.log(anotherMessage);

    const interestRate = .1;
    const principal = 1000;
    console.log(`The interest on your saving is: ${principal * interestRate}`);

// ARRAY DESTRUCTURING
    /**
        It allows us to unpack elements in arrays into distinct variables. allows us to name array
        elements with variables instead of index numbers.

        Syntax:
            let/const [variableName, variableName, variableName,] = array
     */

    const fullName = ["Joe", "Dela", "Cruz"]; 

// Pre-Array Destructuring
// importante positioning ng variable
    console.log()
    console.log(fullName[0]);
    console.log(fullName[1]);
    console.log(fullName[2]);

    console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! its nice to meet you!`);

    console.log()
// Array Destructuring
    const [firstName, middleName, lastName] = fullName
    console.log(firstName);
    console.log(middleName);
    console.log(lastName);
    console.log(`Hello, ${firstName} ${middleName} ${lastName}! its nice to meet you!`);


// Object Destructuring
/**
    allows us to unpack properties of objects into distinct variables. shortens
    the syntax for accessing properties form objects.

    Syntax:
        let/const { propertyName, } = objects
 */

    const person = {
        givenName: "Jane",
        maidenName: "Dela",
        familyName: "Cruz"
    }

    console.log()
// Pre-Object Destructuring
    console.log(person.givenName);
    console.log(person.maidenName);
    console.log(person.familyName);

    console.log()
    const { maidenName, givenName, familyName } = person;
    console.log(givenName);
    console.log(maidenName);
    console.log(familyName);
    

    function getFullName({givenName, maidenName, familyName}) {
        console.log(`${givenName} ${maidenName} ${familyName}`);
    }

    getFullName(person);

// ARROW FUNCTIONS
    /**
        Alternative syntax to traditional functions

        const variableName = () => {
            console.log();
        }
     */

    const hello = () => {
        console.log("Hello")
    }
    hello();

    // TRADITIONAL WAY
    function greetings (){
        console.log("Hello");
    }
    greetings();

// PRE-ARROW FUNCTION
    /**
        Syntax:
            function functionName (parameterA, parameterB) {
                console.log();
            }
     */

    function printFullName (firstName, middleName, lastName) {
        console.log(firstName + "" + middleName + "" + lastName);
    }
    printFullName("Amiel", "D", "Oliva")

// Arrow FUnction
    /**
        Syntax:
            let/const variableName = (parameterA, parameterB) => {
                console.log();
            }
     */

    const printFullName1 = (firstName, middleName, lastName) => {
        console.log(`${firstName} ${middleName} ${lastName}`);
    }
    printFullName1 ("charles partrick", "A", "Ilagan");

    // old way with template literals
    const students = ["Rupert", "Carlos", "Jerome"];

    students.forEach(function(student) {
        console.log(`${student} is a student`);
    });

    // Loop with arrow with template literals
    students.forEach((student) => {
        console.log(`${student} is a student`);
    })

// IMPLICIT RETURN STATEMENT

// Pre-Arrow Function
    function add(x, y) {
        return x + y
    }

    let total = add(12, 15);
    console.log(total);

    /**
     *pag yung task nung function is rekta mag return ng value, kahit wala na return keyword and curly braces.

pag madami pa gagawing tasks sa loob ng function mag curly brace and return keyword na
     */

//Arrow function
    // const addition = (x, y) => {
    //     return x + y
    // };

    const addition = (x, y) => x + y;

    let resultOfAdd = addition (12, 15);
    console.log(resultOfAdd);
    console.log();

// Default Function Argument Value
    const greet = (name = "user") => {
        return `Good Morning, ${name}!`
    }
    console.log(greet());
    console.log(greet("Grace"));
    console.log();

// Class-Based Object Blueprints
    /*
        Allows creation/instatiation of objects using classes as blueprints

        Syntax:
            class className {
                constructor (objectPropertyA, objectPropertyB,) {
                    this.objectPropertyA = objectPropertyA;
                    this.objectPropertyB = objectPropertyB;
                }
            }
    */

    class Car {
        constructor (brand, name, year) {
            this.brand = brand;
            this.name = name;
            this.year = year;
        }
    }

    const myCar = new Car();
    console.log (myCar);
    console.log();

// assigning value to property
    myCar.brand = "Ford";
    myCar.name = "Ranger Raptor";
    myCar.year = "2020";
    console.log(myCar);

    console.log();
    const myNewCar = new Car ("Toyota", "Vios", "2008");
    console.log(myNewCar);